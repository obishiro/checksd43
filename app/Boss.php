<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boss extends Model
{
    //
    protected $table = 'tb_boss';
    public $timestamps = false;
}
