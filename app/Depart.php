<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Depart extends Model
{
    //
    protected $table = 'tb_depart';
    public $timestamps = false;
}
