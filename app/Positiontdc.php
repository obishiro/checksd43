<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Positiontdc extends Model
{
    //
    protected $table = 'tb_position_tdc';
    public $timestamps = false;
}
