<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prov extends Model
{
    //
    protected $table = 'tb_province';
    public $timestamps = false;
}
