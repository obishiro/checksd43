<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amphur extends Model
{
    //
    protected $table = 'tb_amphur';
    public $timestamps = false;
}
