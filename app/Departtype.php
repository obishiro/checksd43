<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departtype extends Model
{
    //
    protected $table = 'tb_depart_type';
    public $timestamps = false;
}
