<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
 
use Illuminate\Support\Facades\Auth;
 
Use App\User;
use App\Prov;
use App\Amphur;
use App\Tumbon;
use App\Depart;
use App\Sd43;
use App\Departtype;
use App\Pol;
use App\Book;
use App\Boss;

Use PDF;


class ExportController extends Controller
{

    public function postExportlistname(Request $request)
    {
        if(Auth::check())
        {
            $txt ='';
            $chkval =$request->input('chkval') ;
           // $in = '(' . implode(',', $chkval) .')';
             
        

                   $data = Sd43::select('tb_check43.depart_id' ,'uname' ,'lname' ,'pid' ,
                    'year_r' ,'tb_district.DISTRICT_NAME','tb_province.Prov_shortname','tb_amphur.AMPHUR_NAME')
                    ->join('tb_province','tb_check43.prov','=','tb_province.PROVINCE_ID')
                    ->join('tb_amphur','tb_check43.amphur','=','tb_amphur.AMPHUR_ID')
                   ->join('tb_district','tb_check43.tumbon','=','tb_district.DISTRICT_CODE')
                    ->whereIn('tb_check43.book_number',$chkval)->orderBy('tb_check43.depart_id','asc')->get();
             
                  $pdf = PDF::loadView('export.listname', array('data'=>$data));
                  $update= sd43::where('year_r','>','2542')->whereIn('book_number',$chkval)->update(['send_to'=>'1']);
                 return @$pdf->stream();
                
                //  $response = array( 
                //     'status' => 1, 
                //     'message' => "บันทึกข้อมูบเรียบร้อยแล้ว"
                // ); 
                // return json_encode($response);
                

        
    }else{
        return redirect('/');
    }
}
    public function getExport($type,$id)
    {
        if(Auth::check())
        {
            switch($type)
            {
                case 'report':
                   $sqldata = Sd43::select('tb_check43.depart_id' ,'uname' ,'lname' ,'pid' ,
                    'year_r' ,'tb_district.DISTRICT_NAME','tb_province.Prov_shortname','tb_amphur.AMPHUR_NAME')
                    ->join('tb_province','tb_check43.prov','=','tb_province.PROVINCE_ID')
                    ->join('tb_amphur','tb_check43.amphur','=','tb_amphur.AMPHUR_ID')
                   ->join('tb_district','tb_check43.tumbon','=','tb_district.DISTRICT_CODE')
                   ->join('tb_book','tb_check43.book_number','=','tb_book.id')
                   ->where('tb_check43.book_number',$id)->orderBy('tb_check43.id','asc')->get();
                    
                      $pdf = PDF::loadView('export.report', array('data'=>$sqldata));
                  
            //      $update= sd43::where('year_r','>','2542')->whereIn('book_number',$chkval)->update(['send_to'=>'1']);
                 return @$pdf->stream();

                
                break;
            }

        
    }else{
        return redirect('/');
    }
    
}

public function getExportword($id)
{
    if(Auth::check())
    {
        // switch($type)
        // {
        //     case 'report':
        //        $sqldata = Sd43::select('tb_check43.depart_id' ,'uname' ,'lname' ,'pid' ,
        //         'year_r' ,'tb_district.DISTRICT_NAME','tb_province.Prov_shortname','tb_amphur.AMPHUR_NAME')
        //         ->join('tb_province','tb_check43.prov','=','tb_province.PROVINCE_ID')
        //         ->join('tb_amphur','tb_check43.amphur','=','tb_amphur.AMPHUR_ID')
        //        ->join('tb_district','tb_check43.tumbon','=','tb_district.DISTRICT_CODE')
        //        ->join('tb_book','tb_check43.book_number','=','tb_book.id')
        //        ->where('tb_check43.book_number',$id)->orderBy('tb_check43.id','asc')->get();
                
        //           $pdf = PDF::loadView('export.report', array('data'=>$sqldata));
              
        // //      $update= sd43::where('year_r','>','2542')->whereIn('book_number',$chkval)->update(['send_to'=>'1']);
        //      return @$pdf->stream();

            
        //     break;
        // }
        
        $wordTest = new \PhpOffice\PhpWord\TemplateProcessor('template.doc');
 
        $newSection = $wordTest->addSection();
     
        $name = "Sontaya";
     
      //  $newSection->addText($name, array('name' => 'Tahoma', 'size' => 15, 'color' => 'black'));
     
        $objectWriter = \PhpOffice\PhpWord\IOFactory::createWriter($wordTest, 'Word2007');
        try {
            $objectWriter->save(storage_path('template.doc'));
        } catch (Exception $e) {
        }
     
        return response()->download(storage_path('template.doc'));
    
}else{
    return redirect('/');
}

}

public function postExportsendtech(Request $request)
    {
        if(Auth::check())
        {
                
                $b = Boss::where('position_id','5')->first();
                $date =date('d-m-Y H-i-s');
                $sum = count($request->input('formdata'));
                $sum_all = cthai($sum);
                $bossname = $b->boss_name;
                $boss_lname = $b->boss_lname;
                $dy = CmonthShort(date('m')).' '.shotyear();
                $filename ='หนังสือปะหน้าส่ง กทส.นรด.-'.$sum.' นาย-'.$date.'.docx';
                // Creating the new document...
                $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(public_path('template/tem_sendtech.docx'));
                $templateProcessor->setValue(
                    ['dy','bossname','boss_lname','sum_person'], [$dy,$bossname,$boss_lname,$sum_all,$date,$filename]);
                
                header("Content-Disposition: attachment; filename=".$filename."");
                $templateProcessor->saveAs($filename);
                //$templateProcessor->saveAs('php://output');

                foreach($request->input('formdata') as $frmdata=>$data)
                {
                        Sd43::where('pid',$data)->update(['booktech_status'=>1]);
                }
                
            
        }else{
            return redirect('/');
        }
    }
    public function postExportsheettech(Request $request)
    {
        if(Auth::check())
        {
            Excel::create('Filename', function($excel) {

                // Call writer methods here
            
            });
        }else{
            return redirect('/');
        }

    }
        



}
