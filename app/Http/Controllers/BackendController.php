<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
Use App\User;
use App\Prov;
use App\Amphur;
use App\Tumbon;
use App\Depart;
use App\Sd43;
use App\Departtype;
use App\Departtypein;
use App\Pol;
use App\Position;
use App\Positiontdc;
use App\Prefix;
use App\Book;
use App\Techfail;


class BackendController extends Controller
{
    public function postLogin(Request $request)
    {
    	$username = $request->input('username');
    	$password = $request->input('password');
    	
       if (Auth::attempt(['username' => $username, 'password' => $password])) 
       {
        $response = array( 
            'status' => 1, 
          //  'message' => "เข้าสู่ระบบตรวจสอบ สด.43"
        );       
       }else{
        $response = array( 
            'status' => 0, 
           // 'message' => 'ชื่อผู้ใช้/รหัสผ่านไม่ถูกต้อง'
        ); 
       }
       return json_encode($response);          
    
    }
    public  function getBackend()
    {
        if(Auth::check())
        {
           $sdcheck = Sd43::select('uname' ,'lname' ,'pid' ,
           'year_r' ,'code_43' ,'sheet_43' ,'code_35' ,'send_to','send_status','boss_sd_status' ,
           'finish_status','tb_province.PROVINCE_NAME','tb_amphur.AMPHUR_NAME')
           ->join('tb_province','tb_check43.prov','=','tb_province.PROVINCE_ID')
           ->join('tb_amphur','tb_check43.amphur','=','tb_amphur.AMPHUR_ID')
        //  ->join('tb_depart', 'tb_check43.depart_id', '=', 'tb_depart.id')
           ->get();
         
           return view('backend',['datasd43'=>$sdcheck]);
        }else{
            return redirect('/');
        }
    }
    public function getBook()
    {
        if(Auth::check())
        {
        $depart = Depart::orderBy('id','asc')->get();
        $prov = Prov::orderBy('PROVINCE_NAME','asc')->get();
        
        $pol = Pol::orderBy('pol_name','asc')->get();
        $book = Book::select('tb_book.id','tb_book.book_number','tb_book.book_date','tb_book.book_secret',
        'tb_book.book_fast','tb_depart.depart_name','tb_depart.depart_code')->orderBy('tb_book.id','desc')
        ->join('tb_depart','tb_book.depart_id','=','tb_depart.id')
        ->get();
        return view('book',['depart'=>$depart,'book'=>$book,'prov'=>$prov,'pol'=>$pol]);
    }else{
        return redirect('/');
    }
    }
    
public function getConfig()
{
    if(Auth::check())
    {
    $depart = Depart::orderBy('depart_name','asc')->get();
    $dep = Departtype::all();
    $dep_in = Departtypein::all();

    $postion = Position::all();
    $positiontdc = Positiontdc::all();

    $prefix = Prefix::all();

    return view('config',[
        'depart'=>$depart,
        'departtype'=>$dep,
        'dep_in'=>$dep_in,
        'position' =>$postion,
        'position_tdc'=>$positiontdc,
        'prefix'=>$prefix
        ]);
    }else{
        return redirect('/');
    }
}
    public function getDepart()
    {
        if(Auth::check())
        {
        $depart = Depart::orderBy('depart_name','asc')->get();
        return view('depart',['depart'=>$depart]);
    }else{
        return redirect('/');
    }
    }
    public function getAdddepart()
    {
        if(Auth::check())
        {
            $dep = Departtype::all();
        return view('adddepart',['departtype'=>$dep]);
        }else{
            return redirect('/');
        }
    }
    

    public function getForm()
    {
        if(Auth::check())
        {
        $prov = Prov::orderBy('PROVINCE_NAME','asc')->get();
        $depart = Depart::orderBy('depart_name','asc')->get();
        $pol = Pol::orderBy('pol_name','asc')->get();
        return view('form',['prov'=>$prov,'depart'=>$depart,'pol'=>$pol]);
        }else{
            return redirect('/');
        }
    }
    public function getSendtech()
    {
        if(Auth::check())
        {
        // $prov = Prov::orderBy('PROVINCE_NAME','asc')->get();
        // $depart = Depart::orderBy('depart_name','asc')->get();
        // $pol = Pol::orderBy('pol_name','asc')->get();
        $sdcheck = Sd43::select('gencode','uname' ,'lname' ,'pid' ,
        'year_r' ,'code_43' ,'sheet_43' ,'code_35','height_val','round_val','tb_pol.pol_name')
        ->join('tb_pol', 'tb_check43.pol_id', '=', 'tb_pol.id')
        ->where('send_to','=','0')
        ->Where('year_r','>', '2542')
        ->OrWhere('booktech_status', '0')
        ->OrWhere('sheettech_status', '0')
        ->orderBy('tb_check43.id','asc')
        ->get();
        return view('sendtech',['datasd43'=>$sdcheck]);
        }else{
            return redirect('/');
        }
    }
    public function getTechreport()
    {
        if(Auth::check())
        {
        // $prov = Prov::orderBy('PROVINCE_NAME','asc')->get();
        // $depart = Depart::orderBy('depart_name','asc')->get();
        // $pol = Pol::orderBy('pol_name','asc')->get();
        $sdcheck = Sd43::select('gencode','uname' ,'lname' ,'pid' ,
        'year_r' ,'code_43' ,'sheet_43' ,'code_35','height_val','round_val','tb_pol.pol_name')
        ->join('tb_pol', 'tb_check43.pol_id', '=', 'tb_pol.id')
        ->where('check_status','0')
        ->where('send_to','1')
        ->Where('booktech_status', '1')
        ->Where('sheettech_status', '1')
        ->orderBy('tb_check43.id','asc')
        ->get();
        return view('techreport',['datasd43'=>$sdcheck]);
        }else{
            return redirect('/');
        }
    }
    public function getTechfail($gencode,$pid)
    {
        if(Auth::check())
        {
        // $prov = Prov::orderBy('PROVINCE_NAME','asc')->get();
        // $depart = Depart::orderBy('depart_name','asc')->get();
        $pol = Pol::orderBy('pol_name','asc')->get();
        $sdcheck = Sd43::select('gencode','uname' ,'lname' ,'pid' ,
        'year_r' ,'code_43' ,'sheet_43' ,'code_35','height_val','round_val','pol_id')
        ->join('tb_pol', 'tb_check43.pol_id', '=', 'tb_pol.id')
        ->where('pid',$pid)
        ->where('gencode',$gencode)
        ->first();
        return view('techfail',['data'=>$sdcheck,'pol'=>$pol,'gencode'=>$gencode]);
        }else{
            return redirect('/');
        }
    }
    public function getCheck($type,$id )
    {
        if(Auth::check())
        {
        switch($type):
            case 'prov':
                $p = Amphur::where('PROVINCE_ID',$id)->orderBy('AMPHUR_NAME','asc')->get();
                    echo "<option value=''>- เลือกข้อมูล -</option>";
                    foreach($p as $pp=>$a)
                    {
                    echo "<option value='".$a->AMPHUR_ID."'>".$a->AMPHUR_NAME."</option> ";
                    }
                     
                    
            break;
            case 'amphur':
                $p = Tumbon::where('AMPHUR_ID',$id)->orderBy('DISTRICT_NAME','asc')->get();
                    echo "<option value=''>- เลือกข้อมูล -</option>";
                    foreach($p as $pp=>$a)
                    {
                    echo "<option value='".$a->DISTRICT_CODE."'>".$a->DISTRICT_NAME."</option> ";
                    }
                     
                    
            break;
            case 'book':
                $p = Book::where('depart_id',$id)->orderBy('id','desc')->get();
                    echo "<option value=''>- เลือกข้อมูล -</option>";
                    foreach($p as $pp=>$a)
                    {
                    echo "<option value='".$a->id."'>".$a->book_date."</option> ";
                    }
                     
                    
            break;
            case 'booknumber':
                $Depart = Book::find($id);
                return $Depart->book_number;
           break;
            case 'depart':
                 $Depart = Depart::find($id);
                 return $Depart->depart_code;
            break;
        endswitch;
    }else{
        return redirect('/');
    }
    }
   
    public function getChecksend($id)
    {
        if(Auth::check())
        {
        $c = Sd43::where(array('book_number'=>$id))->count();
        $data = Sd43::select('tb_check43.depart_id' ,'book_number','book_date' ,'uname' ,'lname' ,'pid' ,
        'year_r' ,'code_43' ,'sheet_43' ,'code_35' ,'send_to','check_status','send_status','boss_sd_status' ,
        'finish_status','tb_depart.depart_name','tb_province.PROVINCE_NAME','tb_amphur.AMPHUR_NAME')
        ->join('tb_province','tb_check43.prov','=','tb_province.PROVINCE_ID')
        ->join('tb_amphur','tb_check43.amphur','=','tb_amphur.AMPHUR_ID')
        ->join('tb_depart', 'tb_check43.depart_id', '=', 'tb_depart.id')->where('book_number',$id)->get();
      
        return view('checksend',['datasd43'=>$data,'count'=>$c]);
        }else{
            return redirect('/');
        }

    }
    public function postChecktech(Request $request)
    {
        if(Auth::check())
        {
           //$update= sd43::where('pid',$request->pid)->update(['check_status'=>'1']);
       
        //    if($update)
        //  {
        //  $response = array( 
        //      'status' => 1, 
        //      'message' => "บันทึกข้อมูบเรียบร้อยแล้ว"
        //  ); 
        //     }else{
        //  $response = array( 
        //      'status' => 0, 
        //      'message' => 'ไม่สามารถบันทึกข้อมูลได้'
        //  ); 
        // }
     //  return json_encode($response);
     $c= count($request->input('pid'));
     $p = $request->input('pid');
     $r = $request->input('pol');
     for($i=0;$i<$c;$i++)
     {
     // return  $request->input('pid')[$i];
      $update= sd43::where('pid',$p[$i])->update(['check_status'=>$r[$i]]);
     }
         if($update)
         {
         $response = array( 
             'status' => 1, 
             'message' => "บันทึกข้อมูลเรียบร้อยแล้ว"
         ); 
            }else{
         $response = array( 
             'status' => 0, 
             'message' => 'ไม่สามารถบันทึกข้อมูลได้'
         ); 
        }
      return json_encode($response);
      
        }else{
            return redirect('/');
        }

    }
    public function PostCheckstatustech(Request $request)
    {
        if(Auth::check())
        {
            
            foreach($request->input('formdata') as $frmdata=>$data)
            {
                $str = explode('-',$data);
                 Sd43::where(
                     array('pid'=>$str[0],'gencode'=>$str[1])
                     )->update(['check_status'=>1]);         
                
            }
            $response = array( 
                'status' => 1, 
                'message' => "บันทึกข้อมูลเรียบร้อยแล้ว"
            ); 

            return json_encode($response);
        }else{
            return redirect('/');
        }
    }

    
    public function PostStatustechfail(Request $request)
    {
        if(Auth::check())
        {
          //  return count($request->input('formdata'));
            
            foreach($request->input('formdata') as $frmdata=>$data)
            {
                $str = explode(',',$data);
                //  Techfail::where(
                //      array('pid'=>$str[0],'gencode'=>$str[1])
                //      )->update(['check_status'=>1]);     
                $t = new Techfail;
                $t->gencode = $str[3];
                $t->pid = $str[2];
                $t->fail = $str[0];
                $t->fail_val = $str[1];

                if($t->save())
                {
                    Sd43::where( array('pid'=>$str[2],'gencode'=>$str[3]))->update(['check_status'=>2]);    
                $response = array( 
                    'status' => 1, 
                    'message' => "บันทึกข้อมูลเรียบร้อยแล้ว"
                ); 
                   }else{
                $response = array( 
                    'status' => 0, 
                    'message' => 'ไม่สามารถบันทึกข้อมูลได้'
                ); 
               }
             
              
            }
            
            return json_encode($response); 
         
        }else{
            return redirect('/');
        }
    }

    public function getExit()
    {
        Auth::logout();
        return redirect('/');
    }
   
 
}
 