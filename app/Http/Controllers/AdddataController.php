<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
Use App\User;
use App\Prov;
use App\Amphur;
use App\Tumbon;
use App\Depart;
use App\Sd43;
use App\Departtype;
use App\Departtypein;
use App\Pol;
use App\Position;
use App\Positiontdc;
use App\Prefix;
use App\Book;
class AdddataController extends Controller
{   
    public function postAdddepart(Request $request)

    {   
        if(Auth::check())
        {
       
    //     $depart = new Book;
    //     $depart->depart_id = $request->input('txt-depart_type_name');
    //     $depart->book_number = $request->input('txt-book_code-number');
    //     $depart->book_date = $request->input('txt-book_date');
    //     $depart->book_secret = $request->input('txt-book-secret');
    //     $depart->book_fast = $request->input('txt-book-fast');
    //     if($depart->save())
    //     {
    //     $response = array( 
    //         'status' => 1, 
    //         'message' => "บันทึกข้อมูบเรียบร้อยแล้ว"
    //     ); 
    // }else{
    //     $response = array( 
    //         'status' => 0, 
    //         'message' => 'ไม่สามารถบันทึกข้อมูลได้'
    //     ); 
      
        
    // }
    //   return json_encode($response);

  
        // if($u){
                 
        //     $response = array( 
        //         'status' => 1, 
        //         'message' => "บันทึกข้อมูบเรียบร้อยแล้ว"
        //     ); 
        // }else{
        //     $response = array( 
        //         'status' => 0, 
        //         'message' => 'ไม่สามารถบันทึกข้อมูลได้'
        //     ); 
        // }
      
                    $depart = new Depart;
                    $depart->depart_type_in = $request->input('txt-depart_type_in');
                    $depart->depart_type = $request->input('txt-depart_type_name');
                    $depart->depart_name = $request->input('txt-depart_name');
                    $depart->depart_code = $request->input('txt-book_code');
                    $depart->depart_address = $request->input('txt-depart_address');
                    $depart->depart_sig_by = $request->input('txt-depart_sig_by');
                    if($depart->save())
                    {
                    $response = array( 'status' => 1, 'message' => "บันทึกข้อมูลเรียบร้อยแล้ว"  ); 
                     }else{
                    $response = array( 'status' => 0, 'message' => 'ไม่สามารถบันทึกข้อมูลได้'); 
                    }
                return json_encode($response);
             
}else{
    return  redirect('/');
}
}
public function postAddperson(Request $request)
{
    if (Auth::check())
    {
     if($request->input('txt_year') <= '2542')
     {
        $sendto ='2';
     }else{
        $sendto ='0';
     }
     $dataarmy = Prov::where(array('PROVINCE_ID'=>$request->input('txt-prov')))->first();

     $date = convdate($request->input('txt-book_date'));

     $countbook = Book::where(array('gencode'=>$request->input('Gencode')))->count();
     if($countbook<=0)
     {
         $book = new Book;
         $book->gencode = $request->input('Gencode') ;
        $book->depart_id = $request->input('txt-depart_type_name');
        $book->book_number = $request->input('txt-book_code-number');
        $book->book_date =  $date;
        $book->book_secret = $request->input('txt-book-secret');
        $book->book_fast = $request->input('txt-book-fast');
     }

     $p = new Sd43;

     $p->gencode = $request->input('Gencode') ;
  

     $p->uname = $request->input('txt-name') ;
     $p->lname = $request->input('txt-lastname');
     $p->pid = $request->input('txt-pid') ;
     $p->army_id = $dataarmy->GEO_ID;
     $p->prov = $request->input('txt-prov');
     $p->amphur = $request->input('txt-amphur');
     $p->tumbon = $request->input('txt-tumbon');
     $p->year_r = $request->input('txt_year') ;
     $p->code_43 = $request->input('txt-sd43_no');
     $p->sheet_43 = $request->input('txt-sd43_sheet');
     $p->code_35 = $request->input('txt-sd35_no');
     $p->height_val = $request->input('txt-height');
     $p->round_val = $request->input('txt-round');
     $p->pol_id = $request->input('txt-pol');
     $p->send_to = $sendto ;
     $p->send_status ='0' ;
     $p->boss_sd_status = '0';
     $p->finish_status = '0';
     $p->created_at = date('Y-m-d H:i:s');
     $p->updated_at = date('Y-m-d H:i:s');

     if($p->save())
     {
     $response = array( 
         'status' => 1, 
         'message' => "บันทึกข้อมูลเรียบร้อยแล้ว"
     ); 
 }else{
     $response = array( 
         'status' => 0, 
         'message' => 'ไม่สามารถบันทึกข้อมูลได้'
     ); 
   
     
 }
   return json_encode($response);

}else{
    return redirect('/');
}

}
}
