<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('login');
});
  
    Route::post('post-login','BackendController@postLogin');
Route::get('backend','BackendController@getBackend');
Route::get('frm','BackendController@getForm');
Route::get('config','BackendController@getConfig');
Route::get('check/{type}/{id}','BackendController@getCheck')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
Route::get('depart','BackendController@getDepart');
Route::get('book','BackendController@getBook');
Route::get('sendtech','BackendController@getSendtech');
Route::get('techreport','BackendController@getTechreport');
Route::get('techfail/{gencode}/{pid}','BackendController@getTechfail');
//Route::post('addbook','BackendController@postAddbook');

Route::post('checkstatustech', 'BackendController@PostCheckstatustech');
Route::post('statustechfail', 'BackendController@PostStatustechfail');


Route::get('checksend/{id}','BackendController@getChecksend')->where(array('id'=>'[0-9]+'));
Route::post('checktech','BackendController@postChecktech');
     
Route::get('exportword/{id}','ExportController@getExportword')->where(array('id'=>'[0-9]+'));
Route::get('exit','BackendController@getExit');

//adddata controller
Route::post('adddepart','AdddataController@postAdddepart');
Route::post('addperson','AdddataController@postAddperson');

//expoty
Route::post('exportlistname','ExportController@postExportlistname');
Route::get('export/{type}/{id}','ExportController@getExport')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
Route::post('exportsendtech','ExportController@postExportsendtech');
Route::post('exportsheettech','ExportController@postExportsheettech');


 

