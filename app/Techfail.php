<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Techfail extends Model
{
    protected $table = 'tb_techfail';
    public $timestamps = false;
  
}
