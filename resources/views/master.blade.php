<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@lang('ui.title')</title>

  <!-- Google Font: Source Sans Pro -->
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Sarabun:wght@200;500&display=swap" rel="stylesheet">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{URL::to('plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="{{URL::to('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{URL::to('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{URL::to('css/adminlte.css')}}">
  <link rel="stylesheet" href="{{URL::to('css/datepicker.css')}}">
 <!-- Select2 -->
 <link rel="stylesheet" href="{{URL::to('plugins/select2/css/select2.min.css')}}">
 <link rel="stylesheet" href="{{URL::to('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{URL::to('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{URL::to('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{URL::to('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
  
  <link rel="stylesheet" href="{{URL::to('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
</head>
<body class="hold-transition layout-top-nav">
  <div class="wrapper">
  
    <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="{{URL::to('img/logo.png')}}" alt="ระบบตรวจสอบ สด.43">
  </div>
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <a class="btn btn-app bg-warning" href="{{URL::to('config')}}">
          
          <i class="fas fa-cogs"></i> ตั้งค่าระบบ
        </a>
        <a class="btn btn-app bg-primary" href="{{URL::to('backend')}}">
          <i class="fas fa-database"></i> ข้อมูลการตรวจสอบ
       </a>
        <a class="btn btn-app bg-success" href="{{URL::to('book')}}">
           <i class="fas fa-edit"></i> บันทึกข้อมูล
        </a>

        <a class="btn btn-app bg-purple" href="{{URL::to('sendtech')}}">
          
          <i class="fas fa-server"></i> ส่ง กทส.นรด.
        </a>
        <a class="btn btn-app bg-teal"  href="{{URL::to('techreport')}}">
          
          <i class="fas fa-id-card"></i> ผล กทส.นรด.
        </a>
        <a class="btn btn-app bg-fuchsia">
          
          <i class="fas fa-check-circle"></i> ตอบผลตรง
        </a>
        <a class="btn btn-app bg-lightblue" href="#">
          <i class="far fa-copy"></i> ตอบผลคู่ขนาน
        </a>
        <a class="btn btn-app bg-navy">
          
          <i class="fas fa-share-square"></i> ส่งกองทัพภาค
        </a>
       
       

        <a class="btn btn-app bg-danger" id="btn-exit"  >
           {{-- href="{{URL::to('exit')}}" --}}
          
          <i class="fas fa-times-circle"></i> ออกจากระบบ
        </a>

      </ul>
  
      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->
        <li class="nav-item">
          <a class="nav-link" data-widget="navbar-search" href="#" role="button">
            <i class="fas fa-search"></i>
          </a>
          
        </li>
  
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-comments"></i>
            <span class="badge badge-danger navbar-badge">3</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
          </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        
       
        <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
            <i class="fas fa-th-large"></i>
          </a>
        </li>
      </ul>
    </nav>
  <!-- /.navbar -->

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   
    <!-- /.content-header -->
<br>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
        @yield('content')
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.1.0
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{URL::to('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{URL::to('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{URL::to('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{URL::to('js/adminlte.js')}}"></script>
 
<script src="{{URL::to('plugins/select2/js/select2.full.min.js')}}"></script>
<script src="{{URL::to('plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{URL::to('plugins/jquery-mousewheel/jquery.mousewheel.js')}}"></script>

<script src="{{URL::to('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{URL::to('plugins/datepicker/bootstrap-datepicker-thai.js')}}"></script>
<script src="{{URL::to('plugins/datepicker/bootstrap-datepicker.th.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{URL::to('js/demo.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{URL::to('js/pages/dashboard2.js')}}"></script>

<script>
  $("#btn-exit").click(function(){
    Swal.fire({
              title: 'ต้องการออกจากระบบงานหรือไม่?',       
              icon: 'info',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'ยืนยัน ออกจากระบบงาน !',
              cancelButtonText: 'ยกเลิก'
            }).then((result) => {
              if (result.isConfirmed) {
                //สร้างรายงาน
               
                Swal.fire({
              title :"ออกจากระบบงานเรียบร้อยแล้ว!",
              icon: 'success',
              showConfirmButton: false,
              timer: 1500
            });
                window.setTimeout(function() {
             window.location.href = "{{URL::to('exit')}}";
              }, 1500);
              }
            })
  });
  </script>
@yield('script')

  
</body>
</html>
