<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>ผนวก ข บัญชีรายละเอียดฯ</title>
    <style>
        @page { margin: 0px; }
        body { margin: 0px; }
        @font-face {
            font-family: 'THSarabun';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabun.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabun';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabun Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabun';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabun Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabun';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabun BoldItalic.ttf') }}") format('truetype');
        }
 
        body {
            font-family: "THSarabun";
            font-size: 16pt;
        }
        .table{
          
            font-size: 14pt;
            padding: 0px;
        }
        td {
             
       border:1 px solid #000 ;
       /* white-space: normal;  */

       
            }
        p {
            font-size: 16pt;
            font-weight: bold;
        }
    </style>
</head>
<body>
<center>
 <p>ผนวก ข บัญชีรายละเอียดใบรับรองผลฯ (แบบ สด.๔๓) ที่ขอตรวจสอบ</p>
</center>
 <table class="table" width="100%" cellpadding ="0" cellspacing="0">
     
     <tr>
     <td align="center"  width="5%" rowspan="2">ลำดับ</td>
     <td align="center" rowspan="2" colspan="2">ชื่อ - สกุล</td>
     <td align="center"  width="10%" rowspan="2" >ตรวจเลือก<br>พ.ศ.</td>
     <td align="center" colspan="3">ภูมิลำเนาทหาร</td>
   
     <td  align="center" width="20%" rowspan="2" style="word-wrap: break-word;">เลขประจำตัว<br>ประชาชน</td>
     <td  align="center" rowspan="2" >หมายเหตุ</td>
  
    </tr>
    <tr>
        
        <td align="center">ตำบล</td>
        <td align="center">อำเภอ</td>
        <td align="center">จังหวัด</td>
       
         
       </tr>
       <?php $i=1; ?>
       @foreach($data as $datas => $d)
       <tr>
        <td  align="center">{{$i}} </td>
        <td style="border-right: 1px solid #fff">&nbsp;นาย{{$d->uname}}</td>
        <td > {{$d->lname}}</td>
        <td  align="center">{{$d->year_r}}</td>
        <td align="center" >{{$d->DISTRICT_NAME}}</td>
      
        <td align="center">{{$d->AMPHUR_NAME}}</td>
        <td align="center">{{$d->Prov_shortname}}</td>
        <td align="center">{{pid_type($d->pid)}}</td>
        <td align="center"></td>
       </tr>
       <?php $i++;?>
       @endforeach
 </table>
 <table width="100%" padding="0" border="0">
     <tr>
         <td style="border:0px" width="50%"></td>
         <td style="border:0px">ตรวจถูกต้อง</td>
     </tr>
 
</table>

</body>
</html>