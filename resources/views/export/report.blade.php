<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>ผนวก ก แบบการตรวจสอบฯ</title>
    <style>
        @page { margin: 15px; }
body { margin: 15px; }
.page-break {
    page-break-after: always;
}
        @font-face {
            font-family: 'THSarabun';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabun.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabun';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabun Bold.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabun';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabun Italic.ttf') }}") format('truetype');
        }
        @font-face {
            font-family: 'THSarabun';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabun BoldItalic.ttf') }}") format('truetype');
        }
 
        body {
            font-family: "THSarabun";
            font-size: 14pt;
        }
        .table{
          
            border: 1 px solid #000;
            padding: 0px;
            
        }
        .txt1 { text-decoration: underline;font-weight: bold}
        .txt {font-weight: bold}
    </style>
</head>
<body>
    @foreach($data as $alldata =>$d)
<center>
 <p style="font-weight: bold">ผนวก ก แบบการตรวจสอบหนังสือสำคัญสายงานสัสดี ประเภทใบรับรองผลฯ (แบบ สด.๔๓) ประกอบหนังสือ นรด. ที่ กห ๐๔๖๒/......ลง...............</p>
</center>
<table class="table" width="98%" cellpadding ="0.5" cellspacing="0.5">
<tr>
    <td>
        <span class="txt1">ข้อมูลผู้รับการตรวจสอบหลักฐาน</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="txt"> เลขประจำตัวประชาชน </span> {{pid_type($d->pid)}}
    </td>
</tr>
<tr><td style="border-bottom: 1px dotted #000">
    ยศ/ชื่อตัว - ชื่อสกุล นาย <span class="txt">{{$d->uname}}</span>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <span class="txt">{{$d->lname}}</span>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     ตรวจเลือก พ.ศ. <span class="txt">{{$d->year_r}}</span>
     
    </td></tr>
    <tr><td style="border-bottom: 1px dotted #000">
        ภูมิลำเนาทหาร ตำบล <span class="txt">{{$d->DISTRICT_NAME}}</span>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         อำเภอ<span class="txt">{{$d->AMPHUR_NAME}}</span>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         ตรวจเลือก พ.ศ. <span class="txt">{{$d->year_r}}</span>
         
        </td></tr>
</table>
<div class="page-break"></div>
@endforeach