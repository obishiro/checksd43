@extends('master')
@section('frm-title')
<i class="fa fa-address-book" aria-hidden="true"></i> @lang('ui.title')
@endsection
 

  </div> 
 
@section('content')
 
<div class="card card-teal">
            <div class="card-header">
              <h3 class="card-title"><i class="fas fa-id-card"></i> ผล กทส.นรด.</h3>
              <div class="card-tools">
                <div class="input-group input-group-sm">
                 
                  <button id="check-true" class="btn btn-success" role="button" aria-pressed="true"><i class="nav-icon fas fa-check-circle fa-lg"></i> ยืนยันผลตรง</button>
               
                  
               
                </div>
              </div>
            </div>
            
            <!-- /.card-header -->
            <div class="card-body">
<table id="example2" class="table table-bordered table-striped table-hover" style="font-size: 13px">
  <thead>
  <tr>
  <th><div  class="icheck-danger d-inline ml-2">
 
    <input type="checkbox" onclick="checkAll(this)" name="todo1" id="todoCheck">
    <label for="todoCheck"></label>
     </div></th>
     <th>รหัส</th>
    <th>เลขบัตร ปชช.</th>
    <th>ชื่อ - นามสกุล</th>
    
    
    <th>ปีตรวจเลือก</th>
    <th>หมายเลขรหัส</th>
    <th>ฉบับที่</th>
    <th>หมายเรียก</th>
    <th>ส่วนสูง</th>
    <th>รอบตัว</th>
    <th>ผลการตรวจเลือก</th>
    <th></th>
   
  </tr>
  </thead>
  <form id="formdata" action="#">
  <tbody>
    <?php $i=1;?>
  @foreach ($datasd43 as $dp=>$d)
      
 
  <tr>
  <td width="5%">  
    {{-- <input type="checkbox" name=""> --}}
    <div class="icheck-danger d-inline">
      <input type="checkbox" class="chbox" id="checkboxDanger{{$i}}" name="box_id"  value="{{$d->pid}}-{{$d->gencode}}">
      <label for="checkboxDanger{{$i}}">
      </label>
    </div>
    </td>
    <td>{{$d->gencode}}</td>
  <td>{{pid_type($d->pid)}}</td>
    <td>นาย{{$d->uname}} {{$d->lname}}
    </td>
  
  <td>{{$d->year_r}}</td>
  <td>{{$d->code_43}}</td>
  <td>{{$d->sheet_43}}</td>
  <td>{{$d->code_35}}</td>
  <td>{{$d->height_val}}</td> 
  <td>{{$d->round_val}}</td> 
  <td>{{$d->pol_name}}</td> 
    <td><a href="{{ URL::to('techfail',array($d->gencode,$d->pid))}}" class="btn btn-danger" role="button" aria-pressed="true"><i class="fas fa-calendar-times fa-lg"></i></a>
    </td>
  
  </tr>
  
  <?php $i++;?>

  @endforeach
  
  </tbody>
  </form>
</table>
</div> </div>
@endsection
@section('script')
<!-- DataTables -->
<script src="{{URL::to('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
  
  $(document).ready(function(e){
    $.ajaxSetup({
      beforeSend: function(xhr, type) {
          if (!type.crossDomain) {
              xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
          }
      },
      });
    
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
     
        "autoWidth": false,
        "responsive": true,
        "pageLength": 50,
        lengthMenu: [
        [ 50,100,150,200, -1 ],
        [ '50', '100', '150','200', 'ทั้งหมด' ]
    ],
    "language": {
            "url": "{{URL::to('plugins/datatables/th.json')}}"
        }
      });
    });

    $('#check-true').click(function(){
    
            if($('.chbox').is(":checked")){
             //สร้างรายงาน
             Swal.fire({
              title: 'ต้องการดำเนินการต่อไปหรือไม่?',       
              icon: 'info',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'ยืนยัน ดำเนินการต่อ!',
              cancelButtonText: 'ยกเลิก'
            }).then((result) => {
              if (result.isConfirmed) {
                //สร้างรายงาน
                var formdata = [];
                   $("input:checkbox[name=box_id]:checked").each(function() {
                        formdata.push($(this).val());
                   });
                 
                $.ajax({
                url: "checkstatustech",
                type: 'post',
                dataType: 'json',
                data: {formdata},
                
                success: function(data) {
                  if(data.status == 1){
                    Swal.fire({
                      title :data.message,
                      icon: 'success'
                    });

                  }else{

                  }
                }
                });
              }
            })
            }
            else if($('.chbox').is(":not(:checked)")){
              Swal.fire({
              title: 'พบข้อผิดพลาด!',
              text: 'เลือกรายชื่อที่จะทำรายการด้วย!',
              icon: 'warning',
              confirmButtonText: 'ตกลง'
              })
            }
       
    });
 
    // $( "#export" ).click(function() {
    //     //$( "#formdata" ).submit();
    //     $.ajax({
    //         url: "example.php",
    //         type: 'post',
    //         dataType: 'application/json',
    //         data: $("#formdata").serialize(),
    //         success: function(data) {
                
    //         }
    // });
    // });
    function checkAll(bx) {
  var cbs = document.getElementsByTagName('input');
  for(var i=0; i < cbs.length; i++) {
    if(cbs[i].type == 'checkbox') {
      cbs[i].checked = bx.checked;
    }
  }
}
  </script>
@endsection