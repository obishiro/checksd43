@extends('master')
@section('frm-title')
<i class="fa fa-address-book" aria-hidden="true"></i> @lang('ui.title')
@endsection
 
@section('content')
 
<div class="card card-warning">
            <div class="card-header">
              <h3 class="card-title"><i class="fas fa-tools"></i> ตั้งค่าระบบ</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              

                <div class="row">
                  <div class="col-12">
                    <!-- Custom Tabs -->
                    <div class="card">
                      <div class="card-header d-flex p-0">
                      
                        <ul class="nav nav-pills ml-auto p-2">
                          <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab"><i class="fas fa-landmark"></i> หน่วยงานที่ขอตรวจสอบ</a></li>
                          <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab"><i class="fas fa-edit"></i> ผู้บังคับบัญชาลงนาม</a></li>
                          <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab"><i class="fas fa-user"></i> ผู้ใช้งานระบบ</a></li>
                          
                        </ul>
                      </div><!-- /.card-header -->
                      <div class="card-body">
                        <div class="tab-content">
<div class="tab-pane active" id="tab_1">
  <form role="form" id="fupForm">
    {{ csrf_field() }}
    <input type="hidden" name="type" value="depart">
  <div class="row"> 
      <div class="col-md-12">
        <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title"><i class="fa fa-building"></i> @lang('ui.depart_name')</h3>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-4">
          <div class="form-group">
            <label for="">หน่วยงาน</label>
      
              <select name="txt-depart_type_in"  class="form-control select2bs4" style="width: 100%;" required>
                <option value="">- เลือกข้อมูล -</option>
                @foreach($dep_in as $dp=>$d)
                <option value="{{$d->id}}">{{$d->dep_in_name}}</option>
                  @endforeach
                 
              </select>
          </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for="">ประเภท @lang('ui.depart_name')</label>
        
                <select name="txt-depart_type_name"     class="form-control select2bs4" style="width: 100%;" required>
                  <option value="">- เลือกข้อมูล -</option>
                  @foreach($departtype as $dp=>$d)
                  <option value="{{$d->id}}">{{$d->depart_type_name}}</option>
                    @endforeach
                   
                </select>
            </div>
            </div>

            <div class="col-sm-4">
                <!-- text input -->
                <div class="form-group">
                  <label>@lang('ui.book_code')</label>
                  <input type="text" name="txt-book_code" class="form-control"  placeholder="" required>
                </div>
              </div>
        </div>         
          
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label for="">@lang('ui.depart_name')</label>
          
                  <input name="txt-depart_name"   id=""  class="form-control" required>
                    
              </div>
              </div>
              <div class="col-sm-6">
                <!-- text input -->
                <div class="form-group">
                  <label>ผู้ลงนามในหนังสือ</label>
                  <input type="text" name="txt-depart_sig_by" class="form-control"  placeholder="" required>
                </div>
              </div>
              
              
                           
            </div>
            <div class="row">
              <div class="col-sm-12">
                <!-- text input -->
                <div class="form-group">
                  <label>@lang('ui.depart_address')</label>
                  <input type="text" name="txt-depart_address" class="form-control"  placeholder="" required>
                </div>
              </div>
             
            </div>
             
              <div class="row">
                <div class="col-sm-12">
                  <div class="statusMsg"></div>
                </div>
              </div>
              <div class="row">

                <div class="col-sm-12">
                  <button class="btn btn-success submitBtn" type="submit" id=""><i class="fa fa-save"></i> @lang('ui.btn-save')</button>
                  <button class="btn btn-danger" type="button" id=""><i class="fa fa-power-off"></i> @lang('ui.btn-cancle')</button>
                </div>
              </div>
      </div>
        </div>
      </div>
             
      </div>
</form>
<div class="row"> 
  <div class="col-md-12">
    <div class="card card-success">
  <div class="card-header">
    <h3 class="card-title"><i class="fa fa-building"></i> ข้อมูล@lang('ui.depart_name')</h3>
  </div>
  <div class="card-body">
    
    <table id="example2" class="table table-bordered table-hover">
      <thead>
      <tr>
      <th>@lang('ui.no')</th>
        <th>@lang('ui.depart_name')</th>
        <th>@lang('ui.book_code')</th>
 
   
        <th>ผู้ลงนาม</th>
      </tr>
      </thead>
      <tbody style="font-size: 14px">
        <?php $i=1;?>
      @foreach ($depart as $dp=>$d)
              
      <tr>
      <td width="5%" align="center">{{$i}}</td>
        <td>{{$d->depart_name}}
        </td>
      <td>{{$d->depart_code}}</td>
    
     
      <td >{{$d->depart_sig_by}}</td>
      </tr>
      <?php $i++;?>
      @endforeach
       
      </tbody>
  </table>
      
      
         
          
      
  </div>
    </div>
  </div>
         
  </div>
 
                          </div>
                          <!-- /.tab-pane -->
                          <div class="tab-pane" id="tab_2">
                            <form role="form" id="fupForm2">
                            {{ csrf_field() }}
                            <input type="hidden" name="type" value="depart">
                          <div class="row"> 
                              <div class="col-md-4">
                                <div class="card card-primary">
                              <div class="card-header">
                                <h3 class="card-title"><i class="fa fa-building"></i> ผู้บังคับบัญชาลงนาม</h3>
                              </div>
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-sm-12">
                                  <div class="form-group">
                                    <label for="">ตำแหน่งคณะกรรมการ</label>
                              
                                      <select name="txt-position"  class="form-control select2bs4" style="width: 100%;" required>
                                        <option value="">- เลือกข้อมูล -</option>
                                        @foreach($position as $pst=>$ps)
                                        <option value="{{$ps->id}}">{{$ps->position_name}}</option>
                                          @endforeach
                                         
                                      </select>
                                  </div>
                                  </div>
                                </div>
                                  <div class="row">
                                  <div class="col-sm-12">
                                    <div class="form-group">
                                      <label for="">ชั้นยศ</label>
                                
                                        <select name="txt-prefix"     class="form-control select2bs4" style="width: 100%;" required>
                                          <option value="">- เลือกข้อมูล -</option>
                                          @foreach($prefix as $prf=>$pr)
                                          <option value="{{$pr->id}}">{{$pr->prefix_name}}</option>
                                            @endforeach
                                           
                                        </select>
                                    </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-12">
                                      <div class="form-group">
                                        <label for="">ตำแหน่ง</label>
                                  
                                          <select name="txt-position_tdc"     class="form-control select2bs4" style="width: 100%;" required>
                                            <option value="">- เลือกข้อมูล -</option>
                                            @foreach($position_tdc as $ptc=>$pt)
                                            <option value="{{$pt->id}}">{{$pt->position_tdc_name}}</option>
                                              @endforeach
                                             
                                          </select>
                                      </div>
                                      </div>
                                    </div>
                                  <div class="row">
                                    <div class="col-sm-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                          <label>ชื่อ - นามสกุล</label>
                                          <input type="text" name="txt-book_code" class="form-control"  placeholder="" required>
                                        </div>
                                      </div>
                                </div>         
                                  
                                  
                                    
                                     
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <div class="statusMsg"></div>
                                        </div>
                                      </div>
                                      <div class="row">
                        
                                        <div class="col-sm-12">
                                          <button class="btn btn-success submitBtn" type="submit" id=""><i class="fa fa-save"></i> @lang('ui.btn-save')</button>
                                          <button class="btn btn-danger" type="button" id=""><i class="fa fa-power-off"></i> @lang('ui.btn-cancle')</button>
                                        </div>
                                      </div>
                              </div>
                                </div>
                              </div>
                              <div class="col-md-8">
                                <div class="card card-success">
                              <div class="card-header">
                                <h3 class="card-title"><i class="fa fa-edit"></i> ข้อมูลผู้บังคับบัญชา</h3>
                              </div>
                              <div class="card-body">
                              </div></div>
                                     
                              </div>
                        </form>
                          </div>
                          <!-- /.tab-pane -->
                          <div class="tab-pane" id="tab_3">
                        
                          </div>
                          <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                      </div><!-- /.card-body -->
                    </div>
                    <!-- ./card -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
            </div></div>
            
@endsection
@section('script')
<!-- DataTables -->
<script src="{{URL::to('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
    $(function () {
    
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "pageLength": 50,
        "autoWidth": false,
        "responsive": true,
        lengthMenu: [
        [ 50,100,150,200, -1 ],
        [ '50', '100', '150','200', 'ทั้งหมด' ]
    ],
    "language": {
            "url": "{{URL::to('plugins/datatables/th.json')}}"
        }
      });
    });
    $('.select2bs4').select2({
  theme: 'bootstrap4'
});

  </script>
  <script>
    $(document).ready(function(e){
      $.ajaxSetup({
        beforeSend: function(xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
            }
        },
    });
        // Submit form data via Ajax
        $("#fupForm").on('submit', function(e){
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: '{{URL::to("adddepart")}}',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function(){
                    $('.submitBtn').attr("disabled","disabled");
                    $('#fupForm').css("opacity",".5");
                },
                success: function(response){ //console.log(response);
                    $('.statusMsg').html('');
                    if(response.status == 1){
                        $('#fupForm')[0].reset();
                        $('.statusMsg').html("<div class='alert alert-info alert-dismissible'>"+
                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                      "<h5><i class='icon fas fa-check'></i> ผลการทำงาน!</h5>"+response.message+'</div>');
                    }else{
                        $('.statusMsg').html("<div class='alert alert-danger alert-dismissible'>"+
                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                      "<h5><i class='icon fas fa-ban'></i> เกิดความผิดพลาด!</h5>"+response.message+'</div>');
                    }
                    $('.statusMsg').hide('300');
                    $('#fupForm').css("opacity","");
                    $(".submitBtn").removeAttr("disabled");
                }
            });
        });
    });
   
 
    </script>
@endsection