<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@lang('ui.title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->

    <link rel="stylesheet" href="{{ URL::to('plugins/all.css')}}">
    <link rel="stylesheet" href="{{URL::to('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <!-- Ionicons -->

  <!-- Ionicons -->

  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ URL::to('plugins/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::to('css/adminlte.css')}}">
  <!-- Google Font: Source Sans Pro -->
 
</head>
 
<body class="hold-transition layout-top-nav" style="background: #f2f2f2">
  <div class="wrapper">
  
    
    <!-- /.navbar -->
    <div class="content-wrapper"  style="background: #f2f2f2">
      <div class="content">
        <div class="container">
<div class="login-box" style="margin-left: 35%;margin-top:15%">


  
            <!-- Input addon -->
            <div class="card card-info ">
              <div class="card-header">
                <h3 class="card-title">@lang('ui.title')</h3>
              </div>
              <div class="card-body">
                <form role="form" id="regForm">
                  {{ csrf_field() }}
                  <center>
                  <img src="{{URL::to('img/logo-tdc.png')}}" alt="">
                </center>
                  <div class="form-group">
                    @if ($errors->has('username'))
                    <span class="badge badge-danger">  <i class="far fa-times-circle"></i> @lang('ui.pls-input')</span>
              
                    @endif
                    
                    <input type="text" required name="username" class="form-control @if ($errors->has('username')) has-error is-invalid @endif" placeholder="ชื่อผู้ใช้งาน..." @if ($errors->has('username')) id="inputError" @endif>
                    <div class="input-group-append">
               
                    </div>
                  </div>
                  <div class="form-group">
                    @if ($errors->has('password'))
                    <span class="badge badge-danger">  <i class="far fa-times-circle"></i> @lang('ui.pls-input')</span>
              
                    @endif
                    <input   required type="password" name="password"class="form-control @if ($errors->has('password')) has-error is-invalid @endif" placeholder="รหัสผ่าน..." @if ($errors->has('password')) id="inputError" @endif>
                    <div class="input-group-append">
               
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-8">
                      <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-sign-in-alt"></i> เข้าสู่ระบบ</button>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                      <button type="reset" class="btn btn-danger btn-block"><i class="fas fa-power-off"></i> ยกเลิก</button>
                    </div>
                    <!-- /.col -->
                  </div>
                  
                </form>
              </div>
              
              <!-- /.card-body -->
            </div>
            <div class="statusMsg"></div>
            <!-- /.card -->
            <!-- Horizontal Form -->
            
 
    {{-- <img src="{{URL::to('img/army.png')}}" style="padding-left: 25%;padding-bottom:5%;width:75%"> --}}

 

<!-- jQuery -->
<script src="{{ URL::to('plugins/jquery.min.js')}}"></script>
 
<!-- AdminLTE App -->
<script src="{{ URL::to('js/adminlte.js')}}"></script>
<script src="{{URL::to('plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<script type="text/javascript">

  $(document).ready(function(e){
      $.ajaxSetup({
        beforeSend: function(xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
            }
        },
    });
    
        // Submit form data via Ajax
        $("#regForm").on('submit', function(e){
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: '{{URL::to("post-login")}}',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function(){
                    // $('.submitBtn').attr("disabled","disabled");
                    // $('#regForm').css("opacity",".5");
                },
                success: function(response){ //console.log(response);
                    $('.statusMsg').html('');
                    if(response.status == 1){
                 
                      //   $('#regForm')[0].reset();
                      //   $('.statusMsg').html("<div class='alert alert-info alert-dismissible'>"+
                      // "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                      // "<h5><i class='icon fas fa-check'></i> ผลการทำงาน!</h5>"+response.message+'</div>');
                      // $('.statusMsg').fadeOut(3500);
                      // window.location.href = "{{URL::to('backend')}}";
                      Swal.fire({
                      title :"ยินดีต้อนรับเข้าสู่ระบบตรวจสอบ\nใบรับรองผลการตรวจเลือกฯ (แบบ สด.43)",
                      icon: 'success',
                      showConfirmButton: false,
                      timer: 1500
                       });
                      window.setTimeout(function() {
                      window.location.href = "{{URL::to('backend')}}";
                      }, 1500);

                    }else{
                        $('.statusMsg').html("<div class='alert alert-danger alert-dismissible'>"+
                      "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                      "<h5><i class='icon fas fa-ban'></i> เกิดความผิดพลาด!</h5>"+response.message+'</div>');
                      $('.statusMsg').fadeOut(2500);
                    }
                    $('#regForm').css("opacity","");
                    $(".submitBtn").removeAttr("disabled");
                }
            });
        });
    });
 </script>  

</body>
</html>
