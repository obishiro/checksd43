@extends('master')
@section('frm-title')
    <i class="fa fa-edit"></i>@lang('ui.frmsave43')
@endsection
@section('content')
<form role="form"  id="Form-Addperson">
  {{ csrf_field() }}
<div class="row"> 
    <div class="col-md-6">
      <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title"><i class="fa fa-building"></i> @lang('ui.depart_name')</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
            <div class="form-group">
              <label for="">@lang('ui.depart_name')</label>
        
                <select name="txt-depart_name"   id="Depart"  class="form-control select2bs4" style="width: 100%;" >
                  <option value="">- เลือกข้อมูล -</option>
                  @foreach($depart as $dp=>$d)
                <option value="{{$d->id}}">{{$d->depart_name}}</option>
                  @endforeach
                </select>
            </div>
            </div>
          </div>
        <div class="row">
        
            <div class="col-sm-5">
              <div class="form-group">
                <label>@lang('ui.date_book')</label>
                <select name="txt-date" id="txt-date" class="form-control select2bs4"></select>
                    
                  </div>
              </div>
              <div class="col-sm-4">
                <!-- text input -->
                <div class="form-group">
                  <label>@lang('ui.book_code')</label>
                  <input type="text" name="txt-book_code" id="Bookcode" class="form-control"  placeholder="" >
                </div>
              </div>
            <div class="col-sm-3">
              <!-- text input -->
              <div class="form-group">
                <label>@lang('ui.book_no')</label>
                
                <input name="txt-book_no" id="txt-book_no" class="form-control">
              </div>
            </div>
           
          </div>
      
           
    </div>
    <!-- /.card-body -->
        </div>
        <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><i class="fa fa-user"></i> @lang('ui.name-basic')</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">
              <!-- text input -->
              <div class="form-group">
                <label>@lang('ui.name')</label>
                <input type="text" name="txt-name" class="form-control" placeholder="" >
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>@lang('ui.lastname')</label>
                <input type="text" name="txt-lastname" class="form-control" placeholder=""  >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
            <div class="form-group">
              <label for="">@lang('ui.pid')</label>
        
                <input type="text" name="txt-pid" class="form-control" placeholder=""  >
            </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <!-- text input -->
              <div class="form-group">
                <label>@lang('ui.prov')</label>
                <select id="Prov"  class="form-control select2bs4" style="width: 100%;" name="txt-prov" >
                  <option value=""></option>
                  @foreach($prov as $pov=>$p)
                  <option value="{{$p->PROVINCE_ID}}">{{$p->PROVINCE_NAME}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>@lang('ui.district')</label>
                <select name="txt-amphur" id="District" class="form-control select2bs4" ></select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <!-- text input -->
              <div class="form-group">
                <label>@lang('ui.tumbon')</label>
                <select name="txt-tumbon" id="Tumbon" class="form-control select2bs4" ></select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>@lang('ui.year43')</label>
                <select name="txt_year" id="" class="form-control select2bs4" >
                  {{GetYear()}}
                </select>
              </div>
            </div>
          </div>
       
    </div>
    <!-- /.card-body -->
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-danger">
    <div class="card-header">
      <h3 class="card-title"><i class="fa fa-book"></i> @lang('ui.frmsd43')</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">
              <!-- text input -->
              <div class="form-group">
                <label>@lang('ui.sd43_no')</label>
                <input type="text" name="txt-sd43_no" class="form-control" placeholder="" >
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>@lang('ui.sd43_sheet')</label>
                <input type="text" name="txt-sd43_sheet" class="form-control" placeholder=""  >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
            <div class="form-group">
              <label for="">@lang('ui.sd35_no')</label>
        
                <input type="text" name="txt-sd35_no" class="form-control" placeholder=""  >
            </div>
            </div>
          </div>
          <div class="row">
          
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>@lang('ui.pol')</label>
                    <select name="txt_pol" id="" class="form-control select2bs4" >
                     <option value="">- เลือกข้อมูล -</option>
                     @foreach ($pol as $pols => $p)
                    <option value="{{$p->id}}">{{$p->pol_name}}</option>
                       
                     @endforeach
                    </select>
                  
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="statusMsg"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <button class="btn btn-success submitBtn" type="submit" id=""><i class="fa fa-save"></i> @lang('ui.btn-save')</button>
              <button class="btn btn-danger" type="reset" id=""><i class="fa fa-power-off"></i> @lang('ui.btn-cancle')</button>
            </div>
          </div>
           
    </div>
    <!-- /.card-body -->
        </div>


        
    </div>



</div>
 
</form>
@endsection
@section('script')
<script type="text/javascript">

$(document).ready(function(e){
    $.ajaxSetup({
      beforeSend: function(xhr, type) {
          if (!type.crossDomain) {
              xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
          }
      },
  });
      // Submit form data via Ajax
      $("#Form-Addperson").on('submit', function(e){
          e.preventDefault();
          $.ajax({
              type: 'POST',
              url: '{{URL::to("addperson")}}',
              data: new FormData(this),
              dataType: 'json',
              contentType: false,
              cache: false,
              processData:false,
              beforeSend: function(){
                  $('.submitBtn').attr("disabled","disabled");
                  $('#Form-Addperson').css("opacity",".5");
              },
              success: function(response){ //console.log(response);
                  $('.statusMsg').html('');
                  if(response.status == 1){
                      $('#Form-Addperson')[0].reset();
                      $('.statusMsg').html("<div class='alert alert-info alert-dismissible'>"+
                    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                    "<h5><i class='icon fas fa-check'></i> ผลการทำงาน!</h5>"+response.message+'</div>');
                  }else{
                      $('.statusMsg').html("<div class='alert alert-danger alert-dismissible'>"+
                    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                    "<h5><i class='icon fas fa-ban'></i> เกิดความผิดพลาด!</h5>"+response.message+'</div>');
                  }
                  $('#Form-Addperson').css("opacity","");
                  $(".submitBtn").removeAttr("disabled");
              }
          });
      });
  });
 
 

  $('#Prov').change(function(){
          		var StrId=$('#Prov').val();
      
              $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "{{URL::to('check/prov')}}/"+StrId,             
                dataType: "html",   //expect html to be returned                
                success: function(response){                    
                    $("#District").html(response); 
                    //alert(response);
                }

    });
  });
  

   		  $('#District').change(function(){
          		var StrId=$('#District').val();
              $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "{{URL::to('check/amphur')}}/"+StrId,             
                dataType: "html",   //expect html to be returned                
                success: function(response){                    
                    $("#Tumbon").html(response); 
                    //alert(response);
                }

    });
  });
  $('#Depart').change(function(){
          		var StrId=$('#Depart').val();
              $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "{{URL::to('check/depart')}}/"+StrId,             
                dataType: "html",   //expect html to be returned                
                success: function(response){                    
                    $("#Bookcode").val(response); 
                    //alert(response);
                }

    });
  });
  $('#Depart').change(function(){
          		var StrId=$('#Depart').val();
              $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "{{URL::to('check/book')}}/"+StrId,             
                dataType: "html",   //expect html to be returned                
                success: function(response){                    
                    $("#txt-date").html(response); 
                    //alert(response);
                }

    });
  });
  $('#txt-date').change(function(){
          		var StrId=$('#txt-date').val();
              $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "{{URL::to('check/booknumber')}}/"+StrId,             
                dataType: "html",   //expect html to be returned                
                success: function(response){                    
                    $("#txt-book_no").val(response); 
                    //alert(response);
                }

    });
  });


//Initialize Select2 Elements
$('.select2bs4').select2({
  theme: 'bootstrap4'
});


 
 
 
  
    
  </script>
 @endsection