@extends('master')
@section('frm-title')
<i class="fa fa-address-book" aria-hidden="true"></i> @lang('ui.title')
@endsection
@section('tools')
<div class="card-tools">
    <div class="input-group input-group-sm">
     
      <a href="{{ URL::to('adddepart')}}" class="btn btn-primary" role="button" aria-pressed="true"><i class="nav-icon fas fa-edit"></i> @lang('ui.btn-add')</a>
      &nbsp;&nbsp;
      <button  class="btn btn-danger" role="button" aria-pressed="true"><i class="nav-icon fas fa-trash"></i> @lang('ui.btn-del')</button>
   
    </div>
  </div> 
@endsection
@section('content')
 
<div class="card card-success">
            <div class="card-header">
              <h3 class="card-title"><i class="fas fa-database"></i> ข้อมูลการตรวจสอบ สด.43</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
<table id="example2" class="table table-bordered table-striped table-hover" style="font-size: 13px">
  <thead>
  <tr>
  <th><div  class="icheck-primary d-inline ml-2">
    <input type="checkbox" value="" name="todo1" id="todoCheck">
    <label for="todoCheck"></label>
     </div></th>
    <th>เลขบัตร ปชช.</th>
    <th>ชื่อ - นามสกุล</th>
    
    <th>หน่วยงานที่ตรวจสอบ</th>
    <th>ปีตรวจเลือก</th>
    <th>หมายเลขรหัส</th>
    <th>ฉบับที่</th>
    <th>ส่ง</th>
    <th>สถานะ</th>
  </tr>
  </thead>
  <tbody>
    <?php $i=1;?>
  @foreach ($datasd43 as $dp=>$d)
      
 
  <tr>
  <td width="5%"> <div  class="icheck-primary d-inline ml-2">
    <input type="checkbox" value="" name="todo1" id="todoCheck{{$i}}">
    <label for="todoCheck{{$i}}"></label>
     </div></td>
  <td>{{pid_type($d->pid)}}</td>
    <td>{{$d->uname}} {{$d->lname}}
    </td>
    <td></td> 
  <td>{{$d->year_r}}</td>
  <td>{{$d->code_43}}</td>
  <td>{{$d->sheet_43}}</td>
  <td></td> 
  <td></td> 
  
  </tr>
  <?php $i++;?>
  @endforeach
   
  </tbody>
</table>
            </div></div>
@endsection
@section('script')
<!-- DataTables -->
<script src="{{URL::to('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
    $(function () {
    
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
@endsection