@extends('master')
@section('frm-title')
<i class="fa fa-address-book" aria-hidden="true"></i> @lang('ui.title')
@endsection
 

  </div> 
 
@section('content')
 
<div class="card card-teal">
            <div class="card-header">
              <h3 class="card-title"><i class="fas fa-id-card"></i> ผล กทส.นรด. ไม่ตรงกับข้อมูลผู้ขอตรวจสอบฯ</h3>
              <div class="card-tools">
                <div class="input-group input-group-sm">
                 
                  <button id="btn-techfail" class="btn btn-danger" role="button" aria-pressed="true"><i class="nav-icon fas fa-calendar-times fa-lg"></i> ยืนยันผลไม่ตรง</button>
               
                  
               
                </div>
              </div>
            </div>
            
            <!-- /.card-header -->
<div class="card-body">
<div class="row">
<div class="col-sm-6">
    <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title"><i class="fa fa-user"></i> @lang('ui.name-basic')</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">
              <!-- text input -->
              <div class="form-group">
                <label>@lang('ui.name')</label>
                <input type="text" name="txt-name" value="{{$data->uname}}" class="form-control" placeholder="" >
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>@lang('ui.lastname')</label>
                <input type="text" name="txt-lastname" value="{{$data->lname}}" class="form-control" placeholder=""  >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="">@lang('ui.pid')</label>
        
                    <input type="text" name="txt-pid" id="old_pid" value="{{$data->pid}}" class="form-control" placeholder=""  >
                </div>
              </div>
            <div class="col-sm-6">
              <!-- text input -->
              <div class="form-group">
                <label>@lang('ui.sd43_no')</label>
                <input type="text" name="txt-sd43_no" value="{{$data->code_43}}" class="form-control cls" placeholder="">
              </div>
            </div>
           
          </div>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label>@lang('ui.sd43_sheet')</label>
                  <input type="text" name="txt-sd43_sheet" value="{{$data->sheet_43}}" class="form-control cls" placeholder="" >
                </div>
              </div>
            <div class="col-sm-6">
            <div class="form-group">
              <label for="">@lang('ui.sd35_no')</label>
        
                <input type="text" name="txt-sd35_no" value="{{$data->code_35}}" class="form-control cls" placeholder="" >
            </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <!-- text input -->
              <div class="form-group">
                <label>ขนาดส่วนสูง</label>
                <input type="text" name="txt-height" value="{{$data->height_val}}" class="form-control cls" placeholder="">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>ขนาดรอบตัว</label>
                <input type="text" name="txt-round" value="{{$data->round_val}}" class="form-control cls" placeholder="" >
              </div>
            </div>
          </div>
          <div class="row">
          
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>@lang('ui.pol')</label>
                    <select name="txt-pol" id="txt-pol-select" class="form-control select2bs4 cls-select">
                     <option>- เลือกข้อมูล -</option>
                     @foreach ($pol as $pols => $p)
                    <option value="{{$p->id}}" @if($data->pol_id == $p->id) selected  @endif>{{$p->pol_name}}</option>
                    
                     @endforeach
                    </select>
                  
              </div>
            </div>
          </div>
           
    </div>
    <!-- /.card-body -->
        </div>
</div>
<div class="col-sm-6">
    <div class="card card-warning">
    <div class="card-header">
      <h3 class="card-title"><i class="fa fa-id-card"></i> ผล กทส.นรด.</h3>
    </div>
    <div class="card-body">
      <form id="formdata" action="#" >
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="icheck-danger d-inline">
                        <input type="checkbox" id="box_no_data" name="box_id" value="1">
                        <label for="box_no_data"><p class="badge badge-danger"> ไม่มีข้อมูลผลการตรวจเลือกฯ</p>
                        </label>
                    </div>
                    
                   
                  </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
              <!-- text input -->
              <div class="form-group">
                <div class="icheck-danger d-inline">
                    <input type="checkbox" class="chbox" id="box_name" value="2">
                      <label for="box_name">@lang('ui.name') </label>
                  </div>
              
               <label for="">ไม่ตรง</label>
                <input type="text" name="txt-name" id="txt-name" value="{{$data->uname}}" class="form-control" placeholder="" disabled  >
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <div class="icheck-danger d-inline">
                    <input type="checkbox" class="chbox" id="box_lname" value="3">
                      <label for="box_lname">@lang('ui.lastname') </label>
                  </div>
              
               <label for="">ไม่ตรง</label>
                <input type="text" name="txt-lastname" id="txt-lastname" value="{{$data->lname}}" class="form-control" placeholder="" disabled   >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <div class="icheck-danger d-inline">
                        <input type="checkbox" class="chbox" id="box_pid" value="4">
                          <label for="box_pid">@lang('ui.pid') </label>
                      </div>
                  
                   <label for="">ไม่ตรง</label>
        
                    <input type="text" name="txt-pid" id="txt-pid" value="{{$data->pid}}" class="form-control" placeholder="" disabled   >
                </div>
              </div>
            <div class="col-sm-6">
              <!-- text input -->
              <div class="form-group">
                <div class="icheck-danger d-inline">
                    <input type="checkbox" class="chbox" id="box_sd43_no" value="5">
                      <label for="box_sd43_no">@lang('ui.sd43_no') </label>
                  </div>
              
               <label for="">ไม่ตรง</label>
                
                <input type="text" name="txt-sd43_no" id="txt-sd43_no" value="{{$data->code_43}}" class="form-control cls" placeholder="" disabled >
              </div>
            </div>
           
          </div>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <div class="icheck-danger d-inline">
                        <input type="checkbox" class="chbox" id="box_sd43_sheet" value="6">
                          <label for="box_sd43_sheet">@lang('ui.sd43_sheet') </label>
                      </div>
                  
                   <label for="">ไม่ตรง</label>
                  
                  <input type="text" name="txt-sd43_sheet" id="txt-sd43_sheet" value="{{$data->sheet_43}}" class="form-control cls" placeholder="" disabled  >
                </div>
              </div>
            <div class="col-sm-6">
            <div class="form-group">
                <div class="icheck-danger d-inline">
                    <input type="checkbox" class="chbox" id="box_sd35_no" value="7">
                      <label for="box_sd35_no">@lang('ui.sd35_no') </label>
                  </div>
              
               <label for="">ไม่ตรง</label>
             
                <input type="text" name="txt-sd35_no" id="txt-sd35_no" value="{{$data->code_35}}" class="form-control cls" placeholder="" disabled  >
            </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <!-- text input -->
              <div class="form-group">
                <div class="icheck-danger d-inline">
                    <input type="checkbox" class="chbox" id="box_height_val" value="8">
                      <label for="box_height_val">ขนาดส่วนสูง </label>
                  </div>
              
               <label for="">ไม่ตรง</label>
               
                <input type="text" name="txt-height_val" id="txt-height_val" value="{{$data->height_val}}" class="form-control cls" placeholder="" disabled>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <div class="icheck-danger d-inline">
                    <input type="checkbox" class="chbox" id="box_round_val" value="9">
                      <label for="box_round_val">ขนาดรอบตัว </label>
                  </div>
                <label>ไม่ตรง</label>
                <input type="text" name="txt-round_val" id="txt-round_val" value="{{$data->round_val}}" class="form-control cls" placeholder="" disabled>
              </div>
            </div>
          </div>
          <div class="row">
          
                <div class="col-sm-12">
                  <div class="form-group">
                    <div class="icheck-danger d-inline">
                        <input type="checkbox" class="chbox" id="box_pol" value="10">
                          <label for="box_pol">@lang('ui.pol') </label>
                      </div>
                    <label> ไม่ตรง</label>
                    <select name="txt-pol" id="txt-pol" class="form-control select2bs4 cls-select" disabled>
                     <option>- เลือกข้อมูล -</option>
                     @foreach ($pol as $pols => $p)
                    <option value="{{$p->id}}">{{$p->pol_name}}</option>
                       
                     @endforeach
                    </select>
                  
              </div>
            </div>
          </div>
          <input type="hidden" id="gencode" value="{{$gencode}}">
      </form>
    </div>
    <!-- /.card-body -->
        </div>
</div>
</div>
 
</div>
 </div>
@endsection
@section('script')
<!-- DataTables -->
<script src="{{URL::to('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
  
  $(document).ready(function(e){
    $.ajaxSetup({
      beforeSend: function(xhr, type) {
          if (!type.crossDomain) {
              xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
          }
      },
    });
    $('#box_no_data').click(function(){
        if(this.checked){
        $("input.chbox").prop("disabled", true);
        }else{ 
            $("input.chbox").prop("disabled", false);
        }
    });
    $('#box_name').click(function(){
        if(this.checked){ $("#txt-name").prop("disabled", false); }else{$("#txt-name").prop("disabled", true); }
    });
    $('#box_lname').click(function(){
        if(this.checked){ $("#txt-lastname").prop("disabled", false); }else{$("#txt-lastname").prop("disabled", true); }
    });
    $('#box_pid').click(function(){
        if(this.checked){ $("#txt-pid").prop("disabled", false); }else{$("#txt-pid").prop("disabled", true); }
    });
    $('#box_sd43_no').click(function(){
        if(this.checked){ $("#txt-sd43_no").prop("disabled", false); }else{$("#txt-sd43_no").prop("disabled", true); }
    });
    $('#box_sd43_sheet').click(function(){
        if(this.checked){ $("#txt-sd43_sheet").prop("disabled", false); }else{$("#txt-sd43_sheet").prop("disabled", true); }
    });
    $('#box_sd35_no').click(function(){
        if(this.checked){ $("#txt-sd35_no").prop("disabled", false); }else{$("#txt-sd35_no").prop("disabled", true); }
    });
    $('#box_height_val').click(function(){
        if(this.checked){ $("#txt-height_val").prop("disabled", false); }else{$("#txt-height_val").prop("disabled", true); }
    });
    $('#box_round_val').click(function(){
        if(this.checked){ $("#txt-round_val").prop("disabled", false); }else{$("#txt-round_val").prop("disabled", true); }
    });
    $('#box_pol').click(function(){
        if(this.checked){ $("#txt-pol").prop("disabled", false); }else{$("#txt-pol").prop("disabled", true); }
    });

     
    $('#btn-techfail').click(function(){
    
    if($('input:checkbox[type=checkbox]:checked').is(":checked")){
     //สร้างรายงาน
     Swal.fire({
      title: 'ต้องการดำเนินการต่อไปหรือไม่?',       
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ยืนยัน ดำเนินการต่อ!',
      cancelButtonText: 'ยกเลิก'
    }).then((result) => {
      if (result.isConfirmed) {
        //สร้างรายงาน
        var formdata = [];
        var old_pid = $('#old_pid').val();
        var gencode = $('#gencode').val();
       // var data ='';
           $("input:checkbox[type=checkbox]:checked").each(function() {
         
              switch ($(this).val()) {
                case '1': data = 'ไม่มีข้อมูลผลการตรวจเลือกฯ'; break;
                case '2': data = $('#txt-name').val(); break;
                case '3': data = $('#txt-lastname').val(); break;
                case '4': data = $('#txt-pid').val(); break;
                case '5': data = $('#txt-sd43_no').val(); break;
                case '6': data = $('#txt-sd43_sheet').val(); break;
                case '7': data = $('#txt-sd35_no').val(); break;
                case '8': data = $('#txt-height_val').val(); break;
                case '9': data = $('#txt-round_val').val(); break;
                case '10': data = $('#txt-pol').val(); break;
              }
            
              formdata.push($(this).val()+','+data+','+old_pid+','+gencode);
               // formdata.push($(this).val());
           });
         
        $.ajax({
        url: "{{URL::to('statustechfail')}}",
        type: 'post',
        dataType: 'json',
        data: {formdata},
        success: function(response) {
     //    alert(response);
          if(response.status == 1){
            Swal.fire({
              title :response.message,
              icon: 'success',
              showConfirmButton: false,
              timer: 1500
            });
          //  window.location.href = "{{URL::to('techreport')}}";
            window.setTimeout(function() {
             window.location.href = "{{URL::to('techreport')}}";
              }, 1500);
          // window.location.reload();

          }else{

          }
        }
        });
      }
    })
    }
    else if($('.chbox').is(":not(:checked)")){
      Swal.fire({
      title: 'พบข้อผิดพลาด!',
      text: 'เลือกรายการที่จะดำเนินการด้วย!',
      icon: 'warning',
      confirmButtonText: 'ตกลง'
      })
    }

});


    
      
    });
 
     
  </script>
@endsection