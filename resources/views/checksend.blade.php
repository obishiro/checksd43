@extends('master')
@section('frm-title')
<i class="fa fa-address-book" aria-hidden="true"></i> @lang('ui.title')
@endsection
 

 
@section('content')
<form role="form" enctype="multipart/form-data"   id="fupForm">
    {{ csrf_field() }}
  <div class="row"> 
      <div class="col-md-12">
        <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title"><i class="fa fa-book"></i> หนังสือ@lang('ui.depart_name')</h3>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-4">
          <div class="form-group">
            <label for="">@lang('ui.depart_name')</label>
      
              <select name="txt-depart_type_name"   id="Depart"  class="form-control select2bs4" style="width: 100%;" required>
                <option value="">- เลือกข้อมูล -</option>
                {{-- @foreach($depart as $dp=>$d)
              <option value="{{$d->id}}">{{$d->depart_name}}</option>
                @endforeach --}}
              </select>
          </div>
          </div>
          <div class="col-sm-4">
            <!-- text input -->
            <div class="form-group">
              <label>@lang('ui.book_code')</label>
              <input type="text" name="txt-book_code" id="Bookcode" class="form-control"  placeholder="" >
            </div>
          </div>
          <div class="col-sm-4">
            <!-- text input -->
            <div class="form-group">
              <label>เลขที่หนังสือ</label>
              <input type="text" name="txt-book_code-number" class="form-control"  placeholder="" required>
            </div>
          </div>
          
                       
        </div>
     
      
          
          
          <div class="row">
            <div class="col-sm-4">
                <!-- text input -->
                <div class="form-group">
                  <label>วันที่ของหนังสือ</label>
                  <input type="text" name="txt-book_date" class="form-control datepicker" data-provide="datepicker" data-date-language="th-th" placeholder="" required>
                  
                </div>
              </div>
            <div class="col-sm-4">
                <div class="form-group">
                  <label for="">ชั้นความลับ</label>
            
                    <select name="txt-book-secret"   class="form-control select2bs4" style="width: 100%;" >
                      <option value="0">- เลือกข้อมูล -</option>
                   
                    <option value="1">ลับ</option>
                    <option value="2">ลับมาก</option>
                    <option value="3">ลับที่สุด</option>
                 
                    </select>
                </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                      <label for="">ชั้นความเร็ว</label>
                
                        <select name="txt-book-fast"   class="form-control select2bs4" style="width: 100%;" >
                            <option value="0">- เลือกข้อมูล -</option>
                   
                            <option value="1">ด่วน</option>
                            <option value="2">ด่วนมาก</option>
                            <option value="3">ด่วนที่สุด</option>
                        </select>
                    </div>
                    </div>
          </div>
           
             
              <div class="row">
                <div class="col-sm-12">
                  <div class="statusMsg"></div>
                </div>
              </div>
              <div class="row">

                <div class="col-sm-12">
                  <button class="btn btn-success submitBtn" type="submit" id=""><i class="fa fa-save"></i> @lang('ui.btn-save')</button>
                  <button class="btn btn-danger" type="button" id=""><i class="fa fa-power-off"></i> @lang('ui.btn-cancle')</button>
                </div>
              </div>
      </div>
        </div>
      </div>
             
      </div>
</form>
<form role="form"  id="fupForm">
  {{ csrf_field() }}
<div class="card card-success">
            <div class="card-header">
              <h3 class="card-title"><i class="fas fa-database"></i> ข้อมูลการตรวจสอบ สด.43</h3>
              <div class="card-tools">
                <div class="input-group input-group-sm">
                 
                  <button id="sendtech" type="submit" class="btn btn-primary submitBtn" role="button" aria-pressed="true"><i class="fas fa-check-circle"></i> ยืนยันผลกับ กทส.</button>
                 
                </div>
              </div> 
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="col-sm-12">
                <div class="statusMsg"></div>
              </div>
              
<table id="example2" class="table table-bordered table-striped table-hover" style="font-size: 13px">
  <thead>
  <tr>
  <th>ลำดับ</th>
    <th>เลขบัตร ปชช.</th>
    <th>ชื่อ - นามสกุล</th>
    
    <th>หน่วยงานที่ตรวจสอบ</th>
    <th>ปีตรวจเลือก</th>
    <th>หมายเลขรหัส</th>
    <th>ฉบับที่</th>
    <th>ผล กทส.</th>
     
  </tr>
  </thead>
  <tbody>
    <?php $i=1;?>
  @foreach ($datasd43 as $dp=>$d)
      
 
  <tr>
  <td width="5%">{{$i}}
  </td>
  <td>{{pid_type($d->pid)}} <input type="hidden" name="pid[]" value="{{$d->pid}}"></td>
    <td>{{$d->uname}} {{$d->lname}}</td>
    <td>{{$d->depart_name}}</td> 
  <td>{{$d->year_r}}</td>
  <td>{{$d->code_43}}</td>
  <td>{{$d->sheet_43}}</td>
  <td><select class="form-control-sm" name="pol[]">
    <option value="">- เลือกข้อมูล -</option>
    <option value="1">ผลตรง</option>
    <option value="2">ผลไม่ตรง</option>
  </select></td> 
 
 
  
  </tr>
  <?php $i++;?>
  @endforeach
   
  </tbody>
</table>
 

            </div></div>
            <input type="hidden" id="_token" value="{{ Session::token() }}">

 </form>
@endsection
@section('script')
<!-- DataTables -->
<script src="{{URL::to('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
  

  $(document).ready(function(e){
    $.ajaxSetup({
      beforeSend: function(xhr, type) {
          if (!type.crossDomain) {
              xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
          }
      },
  });
     // Submit form data via Ajax
     $("#fupForm").on('submit', function(e){
  
          e.preventDefault();
          $.ajax({
              type: 'POST',
              url: '{{URL::to("checktech")}}',
              data: new FormData(this),
              dataType: 'json',
              contentType: false,
              cache: false,
              processData:false,
              beforeSend: function(){
                  $('.submitBtn').attr("disabled","disabled");
                  $('#fupForm').css("opacity",".5");
              },
              success: function(response){ //console.log(response);
                  $('.statusMsg').html('');
                  if(response.status == 1){
                    //  $('#fupForm')[0].reset();
                      $('.statusMsg').html("<div class='alert alert-info alert-dismissible'>"+
                    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                    "<h5><i class='icon fas fa-check'></i> ผลการทำงาน!</h5>"+response.message+'</div>');
                    $('.statusMsg').fadeOut(1500);
                  }else{
                      $('.statusMsg').html("<div class='alert alert-danger alert-dismissible'>"+
                    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                    "<h5><i class='icon fas fa-ban'></i> เกิดความผิดพลาด!</h5>"+response.message+'</div>');
                  }
                  $('#fupForm').css("opacity","");
                  $(".submitBtn").removeAttr("disabled");
              }
          });
      });
  });
 
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
  
       
  </script>
@endsection